// Copyright (c) 2022 wick.zt arg-go is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of
// the Mulan PSL v2. You may obtain a copy of Mulan PSL v2 at:
// http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES
// OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

package arg_test

import (
	"testing"

	"gitee.com/wick.zt/arg-go"
)

func TestPrepareAndParse(t *testing.T) {
	ap, _ := arg.New("argparse", nil)

	type Configure struct {
		A bool   `arg:"-a,<true>"`
		B bool   `arg:"-b"`
		C int8   `arg:"-c,<-37>,:this is a int8 value"`
		D uint   `arg:"-d,<1024>,:keep this to default"`
		E string // skip
		F string `arg:"-f,:leave default empty"`
	}
	cfg := &Configure{}

	if err := ap.Prepare(cfg); err != nil {
		t.Fatal(err)
	}
	t.Logf("Prepared Args: %+v", ap)

	args := []string{"-a", "-b", "-c=-5", "-f", "not empty"}
	if err := ap.Parse(args); err != nil {
		t.Fatal(err)
	}
	t.Logf("Parsed Configure: %+v", cfg)
}

// Copyright (c) 2022 wick.zt arg-go is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of
// the Mulan PSL v2. You may obtain a copy of Mulan PSL v2 at:
// http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES
// OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

package arg_test

import (
	"reflect"
	"testing"
	"time"

	"gitee.com/wick.zt/arg-go"
)

type conf struct {
	Host     string `arg:"--host,<127.0.0.1>,:redis server host"`
	Port     uint16 `arg:"-p,--port,<6379>,:redis server port"`
	Password string `arg:"--password,:redis connection password"`
	Dryrun   bool   `arg:"-d,--dryrun,:do not act if set"`
	Skip     int    // arg parser will skip this field
	Operate  string `arg:"-x,--operate,+r,:operation to act on server"`
}

func TestParse(t *testing.T) {
	ap, _ := arg.New("parse", nil)
	c := &conf{}
	if err := ap.Prepare(c); err != nil {
		t.Fatal(err)
	}
	if err := ap.Parse([]string{"--operate", "skr", "-d", "-p", "12345"}); err != nil {
		t.Error(err)
	}
	c2 := &conf{
		Host:     "127.0.0.1",
		Port:     12345,
		Password: "",
		Dryrun:   true,
		Skip:     0,
		Operate:  "skr",
	}
	if !reflect.DeepEqual(c, c2) {
		t.Errorf("%+v != %+v", c, c2)
	}

	t.Logf("Config: %+v\n", c)
}

func TestHelp(t *testing.T) {
	ap, _ := arg.New("help", nil)
	c := &conf{}
	if err := ap.Prepare(c); err != nil {
		t.Fatal(err)
	}
	defer func() {
		if r := recover(); r != nil {
			t.Log(r)
		}
	}()
	if err := ap.Parse([]string{"-h"}); err != nil {
		t.Fatal(err)
	}
}

func TestRedefine(t *testing.T) {
	var err error
	ap, _ := arg.New("redefine", nil)
	c1 := &conf{}
	c2 := &conf{}
	if err = ap.Prepare(c1); err != nil {
		t.Fatal(err)
	}
	if err = ap.Prepare(c2); err == nil {
		t.Fatal("there should be error")
	}
	if ve, ok := err.(*arg.ValueError); !ok {
		t.Fatal("support *arg.ValueError")
	} else if et := ve.Type(); et != arg.RedefinedArg {
		t.Fatalf("bad value error type: %v", et)
	}
}

type conf2 struct {
	Conf   conf
	Action string `arg:"-a,--action,<create>,:action to move"`
}

func TestUnnamedNestedStruct(t *testing.T) {
	ap, _ := arg.New("unnamed-nested-struct", nil)
	c := &conf2{}
	if err := ap.Prepare(c); err != nil {
		t.Fatal(err)
	}
	t.Logf("Prepared: %+v\n", ap)
	if err := ap.Parse([]string{
		"--operate", "skr",
		"-d",
		"-p", "12345",
		"-a", "delete",
	}); err != nil {
		t.Fatal(err)
	}
	t.Logf("%+v\n", c)
}

type innerConf struct {
	Host     string `arg:"--host,<127.0.0.1>,:redis server host"`
	Port     uint16 `arg:"--port,<6379>,:redis server port"`
	Password string `arg:"--password,:redis connection password"`
	Dryrun   bool   `arg:"--dryrun,:do not act if set"`
	Skip     int    // arg parser will skip this field
	Operate  string `arg:"--operate,+r,:operation to act on server"`
}

type conf3 struct {
	Conf1  innerConf `arg:"--conf1"`
	Conf2  innerConf `arg:"--conf2"`
	Action string    `arg:"-a,--action,<create>,:action to move"`
}

func TestNamedNestedStruct(t *testing.T) {
	ap, _ := arg.New("named-nested-struct", nil)
	c := &conf3{}
	if err := ap.Prepare(c); err != nil {
		t.Fatal(err)
	}
	t.Logf("Prepared: %+v\n", ap)
	if err := ap.Parse([]string{
		"--conf1-operate", "skr",
		"--conf1-dryrun",
		"--conf2-operate", "night",
		"--conf2-port", "12345",
		"-a", "update",
	}); err != nil {
		t.Fatal(err)
	}
	t.Logf("%+v\n", c)
}

func TestParseTimeDuration(t *testing.T) {
	type mock_t struct {
		Wait time.Duration `arg:"-w,--wait"`
	}
	ap, _ := arg.New("parse-time-duration", nil)
	c := &mock_t{}
	if err := ap.Prepare(c); err != nil {
		t.Fatal(err)
	}
	if err := ap.Parse([]string{"-w", "24h"}); err != nil {
		t.Fatal(err)
	}
	if c.Wait != time.Hour*24 {
		t.Errorf("want 24h but got %s", c.Wait)
	}
}

// Copyright (c) 2022 wick.zt arg-go is licensed under Mulan PSL v2.
// You can use this software according to the terms and conditions of
// the Mulan PSL v2. You may obtain a copy of Mulan PSL v2 at:
// http://license.coscl.org.cn/MulanPSL2
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES
// OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PSL v2 for more details.

package arg

import (
	"reflect"
	"testing"
	"time"
)

func TestParseValue(t *testing.T) {
	testcases := []struct {
		s string
		v interface{}
	}{
		{"true", true},
		{"FALSE", false},
		{"-254", int(-254)},
		{"0", uint8(0)},
		{"6666", int16(6666)},
		{"3.14", float32(3.14)},
		{"6666666666", int64(6666666666)},
		{"5s", time.Second * 5},
		{"13m", time.Minute * 13},
		{"2h", time.Hour * 2},
		{"1h23m45s", time.Hour + time.Minute*23 + time.Second*45},
	}
	for i, c := range testcases {
		v, err := parseValue(reflect.TypeOf(c.v), c.s)
		if err != nil {
			t.Error(err)
		}
		if v != c.v {
			t.Errorf("%d: mismatch value: %v != %v", i+1, v, c.v)
		}
		t.Logf("%T %v parsed correctly", v, v)
	}
}
